/* 
   File: utils-test.c
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Suite test file for utils
*/
#include "minunit/minunit.h"
#include "utils/list/element-test.c"
#include "utils/list/control-test.c"
#include "utils/list/getter-test.c"
#include "utils/list/list-test.c"

MU_TEST_SUITE(test_utils) {
    MU_RUN_TEST(test_list_element_new);
    MU_RUN_TEST(test_list_element_data);
    MU_RUN_TEST(test_list_element_pn);

    MU_RUN_TEST(test_list_control_append);
    MU_RUN_TEST(test_list_control_delete);
    MU_RUN_TEST(test_list_control_deleteWDAddr);
    MU_RUN_TEST(test_list_control_insert);
    MU_RUN_TEST(test_list_control_prepend);
    MU_RUN_TEST(test_list_control_purge);
    MU_RUN_TEST(test_list_control_purge_all);
    MU_RUN_TEST(test_list_control_unleash);

    MU_RUN_TEST(test_list_getter_getElement);
    MU_RUN_TEST(test_list_getter_getData);

    MU_RUN_TEST(test_list_foreach);
    MU_RUN_TEST(test_listreverse_foreach);
    MU_RUN_TEST(test_list_count);
    MU_RUN_TEST(test_list_deleteIt);
    MU_RUN_TEST(test_list_newList);
}

