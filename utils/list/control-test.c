/* 
   File: element-test.c
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Test file for control.c
*/

#include "minunit/minunit.h"
#include "utils/list/list.h"
#include "utils/list/control.h"
#include "utils/macros.h"

/* Test creation of new Element */
MU_TEST(test_list_control_append) {
    INT a = 1;
    INT b = 2;
    List* li = newList();
    
    mu_check(li->length == 0);
    mu_check(li->first == NULL);
    mu_check(li->last == NULL);

    li->Append(li, &a);
    mu_check(li->length == 1);
    mu_check(li->first->data == &a);
    mu_check(li->last == li->first);

    li->Append(li, &b);
    mu_check(li->length == 2);
    mu_check(li->last->data == &b);
    mu_check(li->last != li->first);

    li->Purge(li);
    DELETE(li);
}

/* Test remove of Element */
MU_TEST(test_list_control_delete) {
    INT a = 1;
    INT b = 2;
    INT c = 3;    
    List* li = newList();
    mu_check(li->length == 0);

    /* Build list to a->b->c */
    li->Append(li, &a);
    mu_check(li->first->data == &a);
    li->Append(li, &b);
    mu_check(li->last->data == &b);
    li->Append(li, &c);
    mu_check(li->last->data == &c);
    mu_check(li->length == 3);

    /* Delete b, a->c */
    li->Delete(li, 1);
    mu_check(li->length == 2);
    mu_check(li->first->data == &a);
    mu_check(li->last->data == &c);
    mu_check(li->first->next->data == &c);
    mu_check(li->last->previous->data == &a);

    /* Delete c, a */
    li->Delete(li, 1);
    mu_check(li->length == 1);
    mu_check(li->first->data == &a);
    mu_check(li->first->previous == NULL);
    mu_check(li->first->next == NULL);
    mu_check(li->first == li->last);

    /* Delete a */
    li->Delete(li, 0);
    mu_check(li->length == 0);
    mu_check(li->first == li->last);
    mu_check(li->first == NULL);

    /* Try to delete again */
    mu_check(li->Delete(li, 0) == li->length);

    li->Purge(li);
    DELETE(li);
}

/* 
    Test remove of Element 
    Valgrind test the efficiency of the deleting
*/
MU_TEST(test_list_control_deleteWDAddr) {
    INT* a = (INT*)malloc(sizeof(INT));
    INT* b = (INT*)malloc(sizeof(INT));
    INT* c = (INT*)malloc(sizeof(INT));
    List* li = newList();
    mu_check(li->length == 0);

    /* Build list to a->b->c */
    li->Append(li, a);
    mu_check(li->first->data == a);
    li->Append(li, b);
    mu_check(li->last->data == b);
    li->Append(li, c);
    mu_check(li->last->data == c);
    mu_check(li->length == 3);

    /* Delete b, a->c */
    li->DeleteWDAddr(li, b, TRUE);
    mu_check(li->length == 2);
    mu_check(li->first->data == a);
    mu_check(li->last->data == c);
    mu_check(li->first->next->data == c);
    mu_check(li->last->previous->data == a);

    /* Delete c, a */
    li->DeleteWDAddr(li, c, TRUE);
    mu_check(li->length == 1);
    mu_check(li->first->data == a);
    mu_check(li->first->previous == NULL);
    mu_check(li->first->next == NULL);
    mu_check(li->first == li->last);

    /* Delete a */
    li->DeleteWDAddr(li, a, TRUE);
    mu_check(li->length == 0);
    mu_check(li->first == li->last);
    mu_check(li->first == NULL);

    /* Try to delete again */
    mu_check(li->DeleteWDAddr(li, a, TRUE) == li->length);

    DELETE(li);
}

/* Test insertion */
MU_TEST(test_list_control_insert) {
    INT a = 1;
    INT b = 2;
    INT c = 3;
    List* li = newList();
    mu_check(li->length == 0);

    /* Instert a at end, a */
    mu_check(li->Insert(li, &a, 1) == 0);   
    mu_check(li->Insert(li, &a, 0) == 1);  
    mu_check(li->first->data == &a);
    mu_check(li->last->data == &a);

    /* Insert b at end, a->b */
    mu_check(li->Insert(li, &b, 1) == 2);
    mu_check(li->first->data == &a);
    mu_check(li->last->data == &b);
    mu_check(li->first->next->previous == li->first);
    mu_check(li->last->previous->next == li->last);

    /* Insert c at 1, a->c->b */
    mu_check(li->Insert(li, &c, 1) == 3);
    mu_check(li->first->data == &a);
    mu_check(li->first->next->data == &c);
    mu_check(li->last->data == &b);

    li->Purge(li);
    DELETE(li);
}

/* Test Preprend */
MU_TEST(test_list_control_prepend) {
    INT a = 1;
    INT b = 2;
    INT c = 3;
    List* li = newList();
    mu_check(li->length == 0);

    mu_check(li->Prepend(li, &a) == 1);
    mu_check(li->Prepend(li, &b) == 2);
    mu_check(li->Prepend(li, &c) == 3);

    mu_check(li->first->data == &c);
    mu_check(li->last->data == &a);
    mu_check(li->first->next->data == li->last->previous->data);
    mu_check(li->first->next->data == &b);

    li->Purge(li);
    DELETE(li);
}

/* Test Purge list */
MU_TEST(test_list_control_purge) {
    INT a = 1;
    INT b = 2;
    INT c = 3;
    List* li = newList();
    mu_check(li->length == 0);

    mu_check(li->Append(li, &a) == 1);
    mu_check(li->Append(li, &b) == 2);
    mu_check(li->Append(li, &c) == 3);

    li->Purge(li);
    mu_check(li->length == 0);
    mu_check(li->first == NULL);
    mu_check(li->last == NULL);

    DELETE(li);
}

/* 
    Test PurgeAll list 
    Valgrind test the efficiency of the deleting
*/
MU_TEST(test_list_control_purge_all) {
    INT* a = (INT*)malloc(sizeof(INT));
    INT* b = (INT*)malloc(sizeof(INT));
    INT* c = (INT*)malloc(sizeof(INT));
    List* li = newList();
    mu_check(li->length == 0);

    mu_check(li->Append(li, a) == 1);
    mu_check(li->Append(li, b) == 2);
    mu_check(li->Append(li, c) == 3);

    li->PurgeAll(li);
    mu_check(li->length == 0);
    mu_check(li->first == NULL);
    mu_check(li->last == NULL);

    DELETE(li);
}

/* Test unleash element */
MU_TEST(test_list_control_unleash) {
    INT a = 1;
    INT b = 2;
    INT c = 3;
    List* li = newList();

    mu_check(li->length == 0);

    mu_check(li->Append(li, &a) == 1);
    mu_check(li->Append(li, &b) == 2);
    mu_check(li->Append(li, &c) == 3);

    Element* saveAddr = li->first->next;
    unleashElement(li, saveAddr);

    mu_check(li->first->next == li->last);
    mu_check(li->last->previous == li->first);
    mu_check(li->length == 2);

    DELETE(saveAddr);
    li->Purge(li);
    DELETE(li);
}