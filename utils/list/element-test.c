/* 
   File: element-test.c
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Test file for element.c
*/

#include "minunit/minunit.h"
#include "utils/list/element.h"
#include "utils/macros.h"

/* Test creation of new Element */
MU_TEST(test_list_element_new) {
    Element* item = newElement(NULL, NULL, NULL);
    mu_check(item != NULL);

    mu_check(item->data == NULL);
    mu_check(item->next == NULL);
    mu_check(item->previous == NULL);
    DELETE(item);
}

/* Test creation of new Element with data */
MU_TEST(test_list_element_data) {
    INT a = 1;               
    Element* item = newElement(&a, NULL, NULL);
    mu_check(item != NULL);

    mu_check(item->data == &a);
    mu_check(item->next == NULL);
    mu_check(item->previous == NULL);
    DELETE(item);
}

/* Test creation of new Element with previous and next element */
MU_TEST(test_list_element_pn) {
    Element* prev = newElement(NULL, NULL, NULL);
    mu_check(prev != NULL);

    Element* next = newElement(NULL, NULL, NULL);
    mu_check(next != NULL);

    Element* item = newElement(NULL, prev, next);
    mu_check(item != NULL);

    mu_check(item->data == NULL);
    mu_check(item->next == next);
    mu_check(item->previous == prev);
    DELETE(prev);
    DELETE(next);
    DELETE(item);
}