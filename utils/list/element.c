/*
   File: element.c
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Implementation file for element.h
*/

/* relative path to fix include issues */
#include "element.h"

/* Create and return a new element */
Element* newElement(ANY data, Element* previous, Element* next) {
  Element* el = NEW(Element);
  el->data = data;
  el->previous = previous;
  el->next = next;
  return el;
}
