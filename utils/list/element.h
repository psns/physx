/*
    File: element.h
    Author: Nicolas Descamps
    Date: 2020/05/18
    Desc: Header file for List Element
*/

/* relative path to fix include issues */
#include "../macros.h"

#ifndef PHYSX_UTILS_LIST_ELEMENT_H_
#define PHYSX_UTILS_LIST_ELEMENT_H_

typedef struct Element_S Element;

/* Element_S (Element) define an element in a List */
struct Element_S {
  ANY data;
  Element* next;
  Element* previous;
};

/* Create and return new Element */
Element* newElement(ANY data, Element* previous, Element* next);

#endif