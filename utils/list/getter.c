/*
    File: getter.c
    Author: Nicolas Descamps
    Date: 2020/05/18
    Desc: Implementation file fo getter.h
*/

/* relative path to fix include issues */
#include "getter.h"

#include "element.h" /* Provide Element struct */

/* Return the wanted element at the given index (start at 0) */
Element* listGetElement(List* self, DWORD32 idx) {
  if (idx >= self->length)
    return NULL; /* return NULL if idx is outside the List */
  UINT currentIdx = 0;
  Element* elem = self->first;
  while (currentIdx < idx) {
    elem = elem->next;
    currentIdx++;
  }
  return elem;
}

/* Call listGetElement and return only the data inside the element  */
ANY listGetData(List* self, DWORD32 idx) {
  Element* elem = self->GetElement(self, idx);
  return elem == NULL ? NULL : elem->data;
}

/* Call listGetData and return the result casted in INT */
INT32 listGetDataInt(List* self, DWORD32 idx) {
  return GETV(self->GetData(self, idx), INT32);
}

/* Call listGetData and return the result casted in LONG */
LONG listGetDataLong(List* self, DWORD32 idx) {
  return GETV(self->GetData(self, idx), LONG);
}

/* Call listGetData and return the result casted in FLOAT */
FLOAT listGetDataFloat(List* self, DWORD32 idx) {
  return GETV(self->GetData(self, idx), FLOAT);
}

/* Call listGetData and return the result casted in DOUBLE */
DOUBLE listGetDataDouble(List* self, DWORD32 idx) {
  return GETV(self->GetData(self, idx), DOUBLE);
}

/* Call listGetData and return the result casted in STRING */
STRING listGetDataString(List* self, DWORD32 idx) {
  return GETV(self->GetData(self, idx), STRING);
}
