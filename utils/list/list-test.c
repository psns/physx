/* 
    File: list-test.c
    Author: Nicolas Descamps
    Date: 2020/05/19
    Desc: Test file for list main funcitons
*/

#include "minunit/minunit.h"
#include "utils/list/list.h"
#include "utils/macros.h"

VOID forEachTestFnc(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    if (GETV(LIST_GET_ARGS(data, 0), INT) == 2) {
        *stop = TRUE;
        return;
    }
    GETV(e->data, INT) += idx + GETV(LIST_GET_ARGS(data, 0), INT);
    GETV((LIST_GET_ARGS(data, 1)), INT) = GETV(e->data, INT);
}

/* Test foreach function */
MU_TEST(test_list_foreach) {
    INT a = 1;
    INT b = 2;
    INT c = 0;
    INT lastValue = -1;
    VOID* args[] = { &c, &lastValue };
    
    List* li = newList();
    li->Append(li, &a);
    li->Append(li, &b);
    li->Foreach(li, forEachTestFnc, args);

    mu_check(a == 1);
    mu_check(b == 3);
    mu_check(lastValue == b);

    c = 1;
    lastValue = -1;
    li->Foreach(li, forEachTestFnc, args);

    mu_check(a == 2);
    mu_check(b == 5);
    mu_check(lastValue == b);

    c = 2;
    lastValue = -1;
    li->Foreach(li, forEachTestFnc, args);

    mu_check(a == 2);
    mu_check(b == 5);
    mu_check(lastValue == -1);

    li->Purge(li);
    DELETE(li);
}

/* Test list reversed foreach */
MU_TEST(test_listreverse_foreach) {
    INT a = 1;
    INT b = 2;
    INT c = 0;
    INT lastValue = -1;
    VOID* args[] = { &c, &lastValue };

    List* li = newList();
    li->Append(li, &a);
    li->Append(li, &b);
    li->ReverseForeach(li, forEachTestFnc, args);

    mu_check(a == 1);
    mu_check(b == 3);
    mu_check(lastValue == a);

    c = 1;
    lastValue = -1;
    li->ReverseForeach(li, forEachTestFnc, args);

    mu_check(a == 2);
    mu_check(b == 5);
    mu_check(lastValue == a);

    c = 2;
    lastValue = -1;
    li->ReverseForeach(li, forEachTestFnc, args);

    mu_check(a == 2);
    mu_check(b == 5);
    mu_check(lastValue = -1);

    li->Purge(li);
    DELETE(li);
} 

/* Test list Count fnc */
MU_TEST(test_list_count) {
    INT a = 1;
    INT b = 2;
    INT c = 1;
    INT test = 1;
    List* li = newList();

    li->Append(li, &a);
    li->Append(li, &b);
    li->Append(li, &c);
    mu_check(li->Count(li, &test, LT_INT) == 2);

    li->Purge(li);
    DELETE(li);
}

/* Test list deleteIt function */
MU_TEST(test_list_deleteIt) {
    INT a = 1;
    INT b = 2;
    INT c = 1;
    INT test = 1;
    List* li = newList();

    li->Append(li, &a);
    li->Append(li, &b);
    li->Append(li, &c);
    li->DeleteIt(li, &test, LT_INT, TRUE);

    mu_check(li->length == 1);
    mu_check(li->first == li->last);
    mu_check(li->first->data == &b);

    li->Purge(li);
    DELETE(li);
}

/* Test new list function */
MU_TEST(test_list_newList) {
    List* li = newList();

    mu_check(li != NULL);

    mu_check(li->first == NULL);
    mu_check(li->last == NULL);
    mu_check(li->length == 0);

    mu_check(li->Display != NULL);
    mu_check(li->DeleteIt != NULL);
    mu_check(li->Count != NULL);
    mu_check(li->Foreach != NULL);

    mu_check(li->Append != NULL);
    mu_check(li->DeleteIt != NULL);
    mu_check(li->Insert != NULL);
    mu_check(li->Prepend != NULL);
    mu_check(li->Purge != NULL);

    mu_check(li->GetElement != NULL);
    mu_check(li->GetData != NULL);
    mu_check(li->GetDataInt != NULL);
    mu_check(li->GetDataLong != NULL);
    mu_check(li->GetDataFloat != NULL); 
    mu_check(li->GetDataDouble != NULL);
    mu_check(li->GetDataString != NULL);

    DELETE(li);
}