/*
   File: list.h
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Header file for list.c
*/
#ifndef PHYSX_UTILS_LIST_LIST_H_
#define PHYSX_UTILS_LIST_LIST_H_

/* relative path to fix include issues */
#include "../macros.h"
#include "element.h"

/* Get data in a ANY array passed as parameter of a foreach function */
#define LIST_GET_ARGS(var, idx) (*((ANY*)var + idx))

/* Define list type to pass in Display function parameters */
enum ListTypes { LT_INT, LT_LONG, LT_FLOAT, LT_DOUBLE, LT_STRING };

/* Unions */
typedef struct List_S List;

/* List_S (List) define struct to manage element like a List (dynamic array) */
struct List_S {
  Element* first; /* first Element of the List */
  Element* last;  /* last Element of the List  */
  DWORD32 length; /* length of the List (number of Elements) */

  /* Control functions */

  /* Append add Element at the end of the List */
  DWORD32 FUNC(Append)(List*, ANY data);
  /* Delete Element at the given position (start at 0) */
  DWORD32 FUNC(Delete)(List*, DWORD32 idx);
  /* Delete an Element with the given data address */
  DWORD32 FUNC(DeleteWDAddr)(List*, ANY dtAddr, BOOLEAN deldt);
  /* Insert Element at the given postition (start at 0) */
  DWORD32 FUNC(Insert)(List*, ANY data, DWORD32 idx);
  /* Insert an Element at the start of the List */
  DWORD32 FUNC(Prepend)(List*, ANY data);
  /* Purge delete all Element of the List */
  VOID FUNC(Purge)(List*);
  /* Purge elements and pointed data */
  VOID FUNC(PurgeAll)(List*);

  /* Getter functions */

  /* GetElement at the given position (start at 0) */
  Element* FUNC(GetElement)(List*, DWORD32 idx);
  /* GetData of Element at the given position (start at 0) */
  ANY FUNC(GetData)(List*, DWORD32 idx);
  /* Same as GetData but return is casted as INT */
  INT FUNC(GetDataInt)(List*, DWORD32);
  /* Same as GetData but return is casted as LONG */
  LONG FUNC(GetDataLong)(List*, DWORD32);
  /* Same as GetData but return is casted as FLOAT */
  FLOAT FUNC(GetDataFloat)(List*, DWORD32);
  /* Same as GetData but return is casted as DOUBLE */
  DOUBLE FUNC(GetDataDouble)(List*, DWORD32);
  /* Same as GetData but return is casted as STRING */
  STRING FUNC(GetDataString)(List*, DWORD32);

  /* DeleteIt delete an element which matching with given data, if delAll is
   * true,  */
  /* the function will delete all iteration else the first encounter */
  DWORD32 FUNC(DeleteIt)(List*, ANY data, enum ListTypes, BOOLEAN delAll);

  /* Display List element to the console */
  VOID FUNC(Display)(List*, enum ListTypes);

  /* Count the number of iteration where the casted data is present */
  DWORD32 FUNC(Count)(List*, ANY data, enum ListTypes);

  /* Foreach execute a callback for each List Element, 4 parameters will be send
   * to the callback */
  /* e (The element himself) */
  /* idx (The index of the current element start at 0) */
  /* stop (Address of a boolean, false by default, if it's set to true, the
   * function will stop at the end of the current callback) */
  /* data (A parameter which can be passed to callback function) */
  VOID FUNC(Foreach)(List*,
                     VOID FUNC(callback)(Element* e, DWORD32 idx, BOOLEAN* stop,
                                         ANY data),
                     ANY data);

  /* ReverseForeach is the Foreach function but the other way (from end to
   * beginning) */
  VOID FUNC(ReverseForeach)(List*,
                            VOID FUNC(callback)(Element* e, DWORD32 idx,
                                                BOOLEAN* stop, ANY data),
                            ANY data);
};

/* newList create and return an instance of List */
List* newList();

#endif
