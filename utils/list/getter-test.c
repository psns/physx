/* 
    File: getter-test.c
    Author: Nicolas Descamps
    Date: 2020/05/19
    Desc: Test file for list getter functions
*/

#include "minunit/minunit.h"
#include "utils/list/list.h"
#include "utils/macros.h"

/* Test getElement function from getter.c */
MU_TEST(test_list_getter_getElement) {
    INT a = 1;
    INT b = 2;
    List* li = newList();

    li->Append(li, &a);
    li->Append(li, &b);
    mu_check(li->length == 2);
    mu_check(li->GetElement(li, 0)->data == &a);
    mu_check(li->GetElement(li, 1)->data == &b);
    mu_check(li->GetElement(li, 2) == NULL);

    li->Purge(li);
    DELETE(li);
}

MU_TEST(test_list_getter_getData) {
    INT a = 1;
    LONG b = 1;
    FLOAT c = 1.5;
    DOUBLE d = 1.23;
    STRING s = "lol";
    List* li = newList();

    li->Append(li, &a);
    li->Append(li, &b);
    li->Append(li, &c);
    li->Append(li, &d);
    li->Append(li, &s);

    mu_check(GETV(li->GetData(li, 0), INT) == a);
    mu_check(li->GetDataInt(li, 0) == a);
    mu_check(li->GetDataLong(li, 1) == b);
    mu_check(li->GetDataFloat(li, 2) == c);
    mu_check(li->GetDataDouble(li, 3) == d);
    mu_check(li->GetDataString(li, 4) == s);

    li->Purge(li);
    DELETE(li);
}