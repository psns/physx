/*
    File: getter.h
    Author: Nicolas Descamps
    Date: 2020/05/18
    Desc: Header file of base getting functions for List
*/
#ifndef PHYSX_UTILS_LIST_GETTER_H_
#define PHYSX_UTILS_LIST_GETTER_H_

/* relative path to fix include issues */
#include "../macros.h" /* Provide base typedef */
#include "list.h"      /* Provide List struct */

/* local function for List->GetElement */
Element* listGetElement(List* self, DWORD32 idx);
/* local function for List->GetData */
ANY listGetData(List* self, DWORD32 idx);
/* local function for List->GetDataInt */
INT32 listGetDataInt(List* self, DWORD32 idx);
/* local function for List->GetDataLong */
LONG listGetDataLong(List* self, DWORD32 idx);
/* local function for List->GetDataFloat */
FLOAT listGetDataFloat(List* self, DWORD32 idx);
/* local function for List->GetDataDouble */
DOUBLE listGetDataDouble(List* self, DWORD32 idx);
/* local function for List->GetDataString */
STRING listGetDataString(List* self, DWORD32 idx);

#endif
