/*
   File: control.h
   Author: Nicolas Descamps
   Date: 2020/05/17
   Desc: Header file for base control functions for List
*/
#ifndef PHYSX_UTILS_LIST_CONTROL_H_
#define PHYSX_UTILS_LIST_CONTROL_H_

/* relative path to fix include issues */
#include "../macros.h"
#include "list.h"

/* local function for List->Append */
DWORD32 listAppend(List* self, ANY data);
/* local function for List->Delete */
DWORD32 listDelete(List* self, DWORD32 idx);
/* local function for List->DeleteWDAddr */
DWORD32 listDeleteWDAddr(List* self, VOID* dtAddr, BOOLEAN deldt);
/* local function for List->Insert */
DWORD32 listInsert(List* self, ANY data, DWORD32 idx);
/* local function for List->Prepend */
DWORD32 listPrepend(List* self, ANY data);
/* local function for List->Purge */
VOID listPurge(List* self);
/* local function for List->PurgeAll */
VOID listPurgeAll(List* self);
/* local function for List->Merge */
VOID listMerge(List* self, List* lstToAdd);
/* Unleash an Element of the list */
VOID unleashElement(List* self, Element* el);

#endif
