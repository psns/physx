/*
    File: control.c
    Author: Nicolas Descamps
    Date: 2020/05/17
    Desc: Implementation file for control.h
*/

/* relative path to fix include issues */
#include "control.h"

/*
    Add an element at the end of the list
    @return length of list after modifications
*/
DWORD32 listAppend(List* self, ANY data) {
  if (self->length == 0) {
    self->first = newElement(data, NULL, NULL);
    self->last = self->first;
  } else {
    self->last->next = newElement(data, self->last, NULL);
    self->last = self->last->next;
  }
  self->length++;
  return self->length;
}

/*
    Delete an element with the given id
    @return length of list after modifications
*/
DWORD32 listDelete(List* self, UINT idx) {
  if (idx >= self->length) return self->length;
  if (idx == 0) {
    Element* toDel = self->first;
    self->first = self->first->next;
    if (self->first != NULL)
      self->first->previous = NULL;
    else
      self->last = self->first;
    DELETE(toDel);
    self->length--;
  } else {
    Element* elem = self->GetElement(self, idx);
    unleashElement(self, elem);
    DELETE(elem);
  }
  return self->length;
}

/*
    Delete an element with the data address
    @return length of list after modifications
*/
DWORD32 listDeleteWDAddr(List* self, VOID* dtAddr, BOOLEAN deldt) {
  UINT idx = 0;
  Element* current = NULL;
  Element* next = self->first;
  while (idx < self->length && next != NULL) {
    current = next;
    next = next->next;
    if (current->data == dtAddr) {
      unleashElement(self, current);
      if (deldt) DELETE(current->data);
      DELETE(current);
      return self->length;
    }
    idx++;
  }
  return self->length;
}

/*
    Insert an element at the given position
    @return length of list after modifications
*/
DWORD32 listInsert(List* self, ANY data, DWORD32 idx) {
  if (idx > self->length) return self->length;
  if (idx == self->length) return self->Append(self, data);
  if (idx == 0) return self->Prepend(self, data);
  Element* prev = self->GetElement(self, idx - 1);
  Element* elem = newElement(data, prev, prev->next);
  prev->next->previous = elem;
  prev->next = elem;
  self->length++;
  return self->length;
}

/*
    Add an element at the start of the list
    @return length of list after modifications
*/
DWORD32 listPrepend(List* self, ANY data) {
  Element* newElem = newElement(data, NULL, self->first);
  if (self->first != NULL)
    self->first->previous = newElem;
  else
    self->last = newElem;
  self->first = newElem;
  self->length++;
  return self->length;
}

/*
    Delete all element in the list
*/
VOID listPurge(List* self) {
  if (self->length == 0) return;
  Element* current = self->first;
  Element* next;
  do {
    next = current->next;
    DELETE(current);
    current = next;
  } while (current != NULL);
  self->first = NULL;
  self->last = NULL;
  self->length = 0;
}

/*
    Delete all element and data in the list
*/
VOID listPurgeAll(List* self) {
  if (self->length == 0) return;
  Element* current = self->first;
  Element* next;
  do {
    next = current->next;
    if (current->data != NULL) DELETE(current->data);
    DELETE(current);
    current = next;
  } while (current != NULL);
  self->first = NULL;
  self->last = NULL;
  self->length = 0;
}

/*
    Unleash an element from the list
*/
VOID unleashElement(List* self, Element* el) {
  if (el->previous != NULL)
    el->previous->next = el->next;
  else
    self->first = el->next;
  if (el->next != NULL)
    el->next->previous = el->previous;
  else
    self->last = el->previous;
  self->length--;
}
