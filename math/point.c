/*
   File: point.c
   Author: Louis Woisel
   Date: 2020/05/18
   Desc: Implementation file for point.h
*/
#include "point.h"

/* Implementation of math_point_set */
VOID math_point_set(Point* self, DOUBLE x, DOUBLE y) {
  self->x = x;
  self->y = y;
}

/* Implementation of math_point_get */
VOID math_point_get(Point self, DOUBLE* x, DOUBLE* y) {
  *x = self.x;
  *y = self.y;
}

/* Calc the distance between two points */
DOUBLE distanceBetweenPoint(Point p1, Point p2) {
  return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

/* Create and return a new point */
Point newPoint(DOUBLE coord_x, DOUBLE coord_y) {
  Point pt;
  pt.x = coord_x;
  pt.y = coord_y;
  return pt;
}