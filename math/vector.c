/* 
    File: vector.c
    Author: Louis Woisel
    Date: 2020/05/20
    Desc: Implementation file for vector.h
*/
#include <stdio.h>
#include "vector.h"

/* Return the result of the scalar product between the two vector in param */
DOUBLE scalarProduct(Vector* vec1, Vector* vec2){
    return ((vec2->vx - vec1->vx)+(vec2->vy - vec1->vy));
}

/* Multiply the coor of the vector by a scalar */
VOID vectorMultiplyCoordinate(Vector* vec, DOUBLE value){
    vec->vx *= value;
    vec->vy *= value;
}

/* Return the direction angle of the vector */
DOUBLE vectorGetDirectionAngle(Vector* vec){
    return atan2(vec->vy,vec->vx);
}

/* Create a vector using two point */
Vector newVectorByPoint(Point ptA, Point ptB){
    Vector vector;
    vector.vx = ptB.x - ptA.x;
    vector.vy = ptB.y - ptA.y;
    vector.applicationPoint = ptA;

    vector.MultiplyVector = vectorMultiplyCoordinate;

    return vector;
}

/* Create a vector by using an application point and the coord of the vector */
Vector newVectorByCoord(Point applicationPoint, DOUBLE xcoord, DOUBLE ycoord){
    Vector vector;
    vector.vx = xcoord;
    vector.vy = ycoord;
    vector.applicationPoint = applicationPoint;

    vector.MultiplyVector = vectorMultiplyCoordinate;

    return vector;
}