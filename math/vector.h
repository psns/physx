/*
    File: point.h
    Author: Louis Woisel
    Date: 2020/05/18
    Desc: Header file for Point
*/

#include <math.h>

#include "../utils/macros.h"
#include "point.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef PHYSX_MATH_VECTOR_H_
#define PHYSX_MATH_VECTOR_H_

typedef struct Vector_S Vector;

/*Vector_S (Vector) define a Vector*/
struct Vector_S {
  /*coordinate of the Vector*/
  DOUBLE vx;
  DOUBLE vy;

  Point applicationPoint; /*Define the application point of the vector*/

  /* Function that multiply the vetor in param by a scalar */
  VOID FUNC(MultiplyVector)(Vector*, DOUBLE);
};

/*Create a vector between two point*/
Vector newVectorByPoint(Point ptA, Point ptB);
/*Create a point with his application point and his coord*/
Vector newVectorByCoord(Point applicationPoint, DOUBLE xcoord, DOUBLE ycoord);
/* Calc the direction angle of the vector */
DOUBLE vectorGetDirectionAngle(Vector*);
/*Function that calculate the scalar product of
                                  the two vector given in param*/
DOUBLE scalarProduct(Vector*, Vector*);
#endif