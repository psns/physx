/* 
   File: vector-test.c
   Author: Louis Woisel
   Date: 2020/05/20
   Desc: Test file for vector.c
*/

#include "minunit/minunit.h"
#include "vector.h"

/*Test creation of new Vector created by two point*/
MU_TEST(test_vector_new_by_point) {
    Point ptA = newPoint(1,0);
    Point ptB = newPoint(0,1);
    Vector vector = newVectorByPoint(ptA,ptB);
    mu_check(vector.vx == -1);
    mu_check(vector.vy == 1);
}

/*Test creation of new Vector created by an application point and coord*/
MU_TEST(test_vector_new_by_coord) {
    Point ptA = newPoint(1,0);
    Vector vector = newVectorByCoord(ptA,0,0);

    mu_check(vector.applicationPoint.x == ptA.x);
    mu_check(vector.applicationPoint.y == ptA.y);
    mu_check(vector.vx == 0);
    mu_check(vector.vy == 0);
}

/*Test the function that multiply a vector by a scalar*/
MU_TEST(test_vector_multiplication) {
    Point ptA = newPoint(0,1);
    Vector vector = newVectorByCoord(ptA,1,2);
    DOUBLE a = 3;

    vector.MultiplyVector(&vector,a);
    mu_check(vector.vx == 3.0);
    mu_check(vector.vy == 6.0);
}

/* Test the function scalar product */
MU_TEST(test_vector_scalar_product) {
    Point ptA = newPoint(0,1);
    Vector vector1 = newVectorByCoord(ptA,1,1);

    Point ptB = newPoint(0,1);
    Vector vector2 = newVectorByCoord(ptB,2,3);

    mu_check(scalarProduct(&vector1,&vector2) == 3);
}

/* Test the function directional Angle */
MU_TEST(test_vector_get_directional_angle){
    Point ptA = newPoint(0,0);

    Vector vector1 = newVectorByCoord(ptA, 3,3);
    Vector vector2 = newVectorByCoord(ptA, 3,-3);
    Vector vector3 = newVectorByCoord(ptA, -3,3);
    Vector vector4 = newVectorByCoord(ptA, -3,-3);

    mu_check(vectorGetDirectionAngle(&vector1) == M_PI/4);
    mu_check(vectorGetDirectionAngle(&vector2) == -M_PI/4);
    mu_check(vectorGetDirectionAngle(&vector3) == 3*M_PI/4);
    mu_check(vectorGetDirectionAngle(&vector4) == -3*M_PI/4);
}