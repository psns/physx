/*
    File: point.h
    Author: Louis Woisel
    Date: 2020/05/18
    Desc: Header file for Point
*/

/* relative path to fix include issues in v0.6.1 */
#include <math.h>

#include "../utils/macros.h"

#ifndef PHYSX_MATH_POINT_H_
#define PHYSX_MATH_POINT_H_

#define POINT_SET(pt, x, y)                                                  \
  _Pragma(                                                                   \
      "GCC warning \"'POINT_SET' macro is deprecated, use 'math_point_set' " \
      "instead\"") math_point_set(&pt, x, y)
#define POINT_GET(pt, x, y)                                                  \
  _Pragma(                                                                   \
      "GCC warning \"'POINT_GET' macro is deprecated, use 'math_point_get' " \
      "instead\"") math_point_get(pt, x, y)

typedef struct Point_S Point;

/*Point_S (Point) define a point*/
struct Point_S {
  DOUBLE x;
  DOUBLE y;
};

/* Create and return a new point */
Point newPoint(DOUBLE x, DOUBLE y);
/* Return the distance between the two point in param */
DOUBLE distanceBetweenPoint(Point, Point);

/* Function used to change the coord of the point in param */
VOID math_point_set(Point*, DOUBLE x, DOUBLE y);
/* Return the x and y value of the point in param*/
VOID math_point_get(Point, DOUBLE* x, DOUBLE* y);
#endif