/* 
   File: point-test.c
   Author: Louis Woisel
   Date: 2020/05/20
   Desc: Test file for point.c
*/

#include "minunit/minunit.h"
#include "point.h"

/* Test creation of new Point */
MU_TEST(test_point_new) {
    Point item = newPoint(1,1);

    mu_check(item.x == 1);
    mu_check(item.y == 1);
}

/* Test point getter */
MU_TEST(test_point_get) {
    Point item = newPoint(1,1);
    DOUBLE x; DOUBLE y;
    math_point_get(item, &x, &y);

    mu_check(x == 1);
    mu_check(y == 1);
}

/* Test point setter */
MU_TEST(test_point_set) {
    Point item = newPoint(1,1);
    math_point_set(&item, 2, 2);

    mu_check(item.x == 2);
    mu_check(item.y == 2);
}


/* Test point macro POINT_GET, POINT_SET */
MU_TEST(test_point_macro) {
    Point item = newPoint(1, 1);
    math_point_set(&item, 2, 2);

    mu_check(item.x == 2);
    mu_check(item.y == 2);

    DOUBLE testX; DOUBLE testY;
    math_point_get(item, &testX, &testY);

    mu_check(testX == 2);
    mu_check(testY == 2);
}