/* 
    File: math-test.c
    Author: Louis Woisel
    Date: 2020/05/20
    Desc: File with the test suite for math

*/

#include "minunit/minunit.h"
#include "point-test.c"
#include "vector-test.c"

MU_TEST_SUITE(test_math) {
    MU_RUN_TEST(test_point_new);
    MU_RUN_TEST(test_point_get);
    MU_RUN_TEST(test_point_set);
    MU_RUN_TEST(test_point_macro);
    MU_RUN_TEST(test_vector_new_by_point);
    MU_RUN_TEST(test_vector_new_by_coord);
    MU_RUN_TEST(test_vector_multiplication);
    MU_RUN_TEST(test_vector_scalar_product);
    MU_RUN_TEST(test_vector_get_directional_angle);
}