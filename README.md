# Physx

Physx est le module qui contient les éléments nécessaires à la partie simulation de l'application.

Il contient 3 partie : 
* `math` : utilisé pour la création de point et de vecteur, ansi que les opérations nécessaires sur ces derniers
* `aqua` : utilisé pour la création de charge et pour les calculs liées à la loi/force de Coulomb
* `utils` : contient les fonctions de créations et de traitements de liste chainée
* `miunit` : librairie utilisée pour effectuer les test unitaire des différentes fonctions du module physx

## Math

La partie math contient deux aspects :

* La création de points en deux dimensions par le biais d'une structure, ainsi que les fonction permettants la modification de leurs coordonnées.

```c
// file : math/point.h
typedef struct Point_S Point;
struct Point_S {
        DOUBLE x;      
        DOUBLE y;

        VOID FUNC(Set) (Point*, DOUBLE x, DOUBLE y);   /* Function used to change the coord of the point in param */
        VOID FUNC(Get) (Point, DOUBLE* x, DOUBLE* y); /* Return the x and y value of the point in param*/
};
```
*  La création de vecteurs ainsi que l'implémentation des opérations vectorielles nécessaires à l'application.

```c
// file : math/vector.h
typedef struct Vector_S Vector;
struct Vector_S {
        /*coordinate of the Vector*/
        DOUBLE vx;
        DOUBLE vy;

        Point applicationPoint;                                     /*Define the application point of the vector*/

        VOID    FUNC(MultiplyVector)        (Vector*,DOUBLE);        /* Function that multiply the vetor in param by a scalar */
        DOUBLE  FUNC(GetNorm)               (Vector*);               /* Calc the norm of the vector in param */
        DOUBLE  FUNC(GetDirectionAngle)     (Vector*);               /* Calc the direction angle of the vector */
};
```
## Aqua

La partie Aqua contient :

* Les éléments nécessaires à la création d'une charge
```c
// file : charge.h
typedef struct Charge_S Charge;
struct Charge_S {
        Point pos;          /* Position of the charge */
        DOUBLE q;           /* q value of the charge */
};
```

* Les fonctions nécesaires au calcul de la force de Coulomb exercée par une charge sur une autre
```c
// file : columbia.h
DOUBLE coulombLaw(Charge c1, Charge c2);          /* calculate the coulomb law between two charge */
Vector* calcCoulombForce(Charge c1, Charge c2);   /* calculate the coulomb force vector */
```

## Utils

La partie utils contient les éléments nécessaires à la création et au traitement des listes chainées. Elle contient également un fichier "macro.h" où sont définit les types standards utilisés et quelques abréviations.

## Minunit

La partie minunit contient la librairie externe utilisée pour réaliser les tests unitaires des fonctions du module physx. Elle possède son propre README.md pour le détail de son fonctionnement.