/*
    File: charge.h
    Author: Louis Woisel
    Date: 2020/05/22
    Desc: Header file for charge
*/

#include "../math/point.h"
#include "../utils/macros.h"

#ifndef PHYSX_AQUA_CHARGE_H_
#define PHYSX_AQUA_CHARGE_H_

#define QELECTRON -1.6E-16
#define QPROTON 1.6E-16

typedef struct Charge_S Charge;

/*Charge_S (Charge) define a charge*/
struct Charge_S {
  Point pos; /* Position of the charge */
  DOUBLE q;  /* q value of the charge */
};

/* Create and return a new charge */
Charge newCharge(DOUBLE x, DOUBLE y, DOUBLE qValue);
#endif