/* 
   File: charge-test.c
   Author: Louis Woisel
   Date: 2020/05/22
   Desc: Test file for charge.c
*/

#include "minunit/minunit.h"
#include "aqua/charge.h"

/* Test creation of new Charge */
MU_TEST(test_charge_new) {
    Charge item = newCharge(1, 1, QPROTON);

    mu_check(item.pos.x == 1);
    mu_check(item.pos.y == 1);
    mu_check(item.q=QPROTON);
}