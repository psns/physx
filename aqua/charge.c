/* 
    File: charge.c
    Author: Louis Woisel
    Date: 2020/05/22
    Desc: implementation of charge.h
*/

#include "charge.h"

/* Create and return a new Charge */
Charge newCharge(DOUBLE x, DOUBLE y, DOUBLE qValue){
    Charge charge;
    charge.pos = newPoint(x, y);
    charge.q = qValue;
    return charge;
}