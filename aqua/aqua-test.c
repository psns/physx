/* 
    File: aqua-test.c
    Author: Louis Woisel
    Date: 2020/05/22
    Desc: File with the test suite for aqua

*/

#include "minunit/minunit.h"
#include "charge-test.c"
#include "columbia-test.c"


MU_TEST_SUITE(test_aqua) {
    MU_RUN_TEST(test_charge_new);
   /*  MU_RUN_TEST(test_coulomb_law);
    MU_RUN_TEST(test_coulomb_force); */
}