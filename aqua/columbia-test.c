/* 
    File: columbia-test.c
    Author: Louis Woisel
    Date: 2020/05/28
    Desc: File with test function for columbia
*/

#include "minunit/minunit.h"
#include "aqua/columbia.h"

//Function tested with #define COULOMB_CONST (9E+9)
/* MU_TEST(test_coulomb_law) {
    Charge q1 = newCharge(0, 0, QELECTRON);
    Charge q2 = newCharge(0, 1E-7, QPROTON);

    printf("\n%e", coulombLaw(q1, q2)); // ~ -2.304E-8
} */

//Function tested with #define COULOMB_CONST (8E+9)
/* MU_TEST(test_coulomb_force) {
    Charge q1 = newCharge(0, 0, CT_POS);
    Charge q2 = newCharge(0.7, 0.7, CT_POS);

    Vector resultCoulombForce = calcCoulombForce(q1,q2);
    printf("\n%lf",resultCoulombForce.vx);
    printf("\n%lf",resultCoulombForce.vy);
}  */