/*
    File: columbia.h
    Author: Louis Woisel
    Date: 2020/05/25
    Desc: Header File for columbia
*/

#include <math.h>

#include "../aqua/charge.h"
#include "../math/vector.h"
#include "../utils/macros.h"

#ifndef PHYSX_AQUA_COLUMBIA_H_
#define PHYSX_AQUA_COLUMBIA_H_

#define COULOMB_CONST (8.987551787368176E+9)
//#define COULOMB_CONST (9E+9)

/* calculate the coulomb law between two charge */
DOUBLE coulombLaw(Charge c1, Charge c2);
/* calculate the coulomb force vector */
Vector calcCoulombForce(Charge c1, Charge c2);

#endif