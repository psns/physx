/*
    File: columbia.c
    Author: Louis Woisel
    Date: 2020/05/25
    Desc: Implementation File for Columbia
*/

#include "columbia.h"

/* Implementation of coulombLaw */
DOUBLE coulombLaw(Charge c1, Charge c2) {
  return COULOMB_CONST * (c1.q * c2.q) /
         pow(distanceBetweenPoint(c1.pos, c2.pos), 2);
}

/* Implementation of calcCoulombForce */
Vector calcCoulombForce(Charge c1, Charge c2) {
  /* Create a vector between the two charge */
  Vector vectorC1C2 = newVectorByPoint(c1.pos, c2.pos);

  /* get the angle teta of the vector */
  DOUBLE angle = vectorGetDirectionAngle(&vectorC1C2);

  /* get the result of the coulomb law */
  DOUBLE resultCoulombLaw = coulombLaw(c1, c2);

  /* Calc the coord of the coulomb force vector */
  DOUBLE fx = -cos(angle) * resultCoulombLaw;
  DOUBLE fy = -sin(angle) * resultCoulombLaw;

  /* create the coulomb force vector and return it*/
  return newVectorByCoord(c1.pos, fx, fy);
  ;
}