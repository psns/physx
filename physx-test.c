// File: physx-test.c
// Author: Nicolas Descamps
// Date: 2020/05/16
// Desc: physx main test file
#include "minunit/minunit.h"
#include "utils/macros.h"
#include "utils/utils-test.c"
#include "math/math-test.c"
#include "aqua/aqua-test.c"

INT main(INT argc, STRING argv[]) {
	MU_RUN_SUITE(test_utils);
	MU_RUN_SUITE(test_math);
	MU_RUN_SUITE(test_aqua);
	MU_REPORT();
	return MU_EXIT_CODE;
}