COMPILER = gcc
LINKER = ar cr
FLAGS = -Wall -std=c99 -I ${shell pwd} -Wno-unused-variable -Wno-unused-function -lm
LIB_NAME = physx.a
TEST_OUTPUT_NAME = physx.test

all: before physx.a

install:
	git submodule update --init

update:
	git submodule update --remote

before:
	mkdir -p bin

# Start test 
test: before physx.a
	${COMPILER} physx-test.c -L. ${LIB_NAME} -o ${TEST_OUTPUT_NAME} ${FLAGS}
	valgrind --leak-check=yes --error-exitcode=1 ./${TEST_OUTPUT_NAME}

# Create a static lib 
physx.a: \
		physx.a-utils-list-element \
		physx.a-utils-list-control \
		physx.a-utils-list-getter \
		physx.a-utils-list \
		physx.a-math-point \
		physx.a-math-vector \
		physx.a-aqua-charge \
		physx.a-aqua-columbia
	${LINKER} physx.a bin/physx-*.o
	
physx.a-utils-list-element:
	${COMPILER} -c utils/list/element.c -o bin/physx-utils-list-element.o ${FLAGS}

physx.a-utils-list-control:
	${COMPILER} -c utils/list/control.c -o bin/physx-utils-list-control.o ${FLAGS}

physx.a-utils-list-getter:
	${COMPILER} -c utils/list/getter.c -o bin/physx-utils-list-getter.o ${FLAGS}

physx.a-utils-list:
	${COMPILER} -c utils/list/list.c -o bin/physx-utils-list.o ${FLAGS}

physx.a-math-point:
	${COMPILER} -c math/point.c -o bin/physx-math-point.o ${FLAGS}

physx.a-math-vector:
	${COMPILER} -c math/vector.c -o bin/physx-math-vector.o ${FLAGS}

physx.a-aqua-charge:
	${COMPILER} -c aqua/charge.c -o bin/physx-aqua-charge.o ${FLAGS}

physx.a-aqua-columbia:
	${COMPILER} -c aqua/columbia.c -o bin/physx-aqua-columbia.o ${FLAGS}

# Delete all .a files
clean:
	rm -rf *.a
	rm -rf *.test
	rm -rf bin